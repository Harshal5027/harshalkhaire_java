class WhileDemo{
	public static void main(String[]args){
		int num=93079226;
		int count=0;
		while(num>0){
			count++;
			num=num/10;
		}
		System.out.println("Counts of digits =" + count);
	}
}
