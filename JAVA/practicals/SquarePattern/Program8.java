import java.util.*;
class SquareDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter no of rows : ");
		int row=sc.nextInt();

		for(int i=1; i<=row; i++){
			int num=64+row;
			for(int j=1; j<=row; j++){
				if(i%2==1){
					if(j%2==1){
						System.out.print("#"+" ");
					}else{
						System.out.print((char)num-- +" ");
					}
				}else{
					if(j%2==0){
                                                System.out.print("#"+" ");
                                        }else{
                                                System.out.print((char)num-- +" ");
                                        }
				}
			}
			System.out.println();
		}
	}
}
