import java.util.*;
class PyramidDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows : ");
		int rows=sc.nextInt();

		for(int i=1; i<=rows; i++){
			int num1=65;
			int num2=97;
			for(int sp=1; sp<=rows-i; sp++){
				System.out.print("\t");
			}
			for(int j=1; j<=i*2-1; j++){
				if(i%2==1){
					if(i>j){
						System.out.print((char)num1++ +"\t");
					}else{
						System.out.print((char)num1-- +"\t");
					}
				}else{
					if(i>j){
                                                System.out.print((char)num2++ +"\t");
                                        }else{
                                                System.out.print((char)num2-- +"\t");
                                        }
				}
			}
			System.out.println();
		}
	}
}

