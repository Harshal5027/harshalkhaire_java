import java.util.*;
class MixedDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter rows : ");
                int row=sc.nextInt();

                for(int i=1; i<=row; i++){
                        int ch=64+(row-i+1);
                        for(int j=1; j<=row-i+1; j++){
                                if(i%2==0){
                                        System.out.print((char)ch-- +" ");
                                }else{
                                        System.out.print(j +" ");
                                }
                        }
                        System.out.println();
                }
        }
}
