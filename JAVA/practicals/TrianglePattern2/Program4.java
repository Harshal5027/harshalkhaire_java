import java.io.*;
class InputDemo{
        public static void main(String[]args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row=Integer.parseInt(br.readLine());
		
                for(int i=1; i<=row; i++){
			int num1=64+row;
                	int num2=96+row;
                        for(int j=1; j<=i; j++){
				if(i%2==0){
					System.out.print((char)num1-- +" ");
				}else{
					System.out.print((char)num2-- + " ");
				}
                        }
                        System.out.println();
                }
        }
}
