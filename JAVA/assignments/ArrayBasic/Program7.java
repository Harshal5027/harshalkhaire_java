import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an Array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.print("Enter the element : ");
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Given array elements are : ");
		for(int i=0;i<size;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		System.out.println("Number divisible by 4 are : ");
		for(int i=0; i<size; i++){
			if(arr[i]%4==0){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
	}
}
