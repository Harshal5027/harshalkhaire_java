import java.util.*;
class ArrayDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
		System.out.print("Enter array element :");
		int size=sc.nextInt();
                int arr[]=new int[size];

                for(int i=0; i<size; i++){
                        System.out.print("Enter Element : ");
                        arr[i]=sc.nextInt();
                }
		System.out.println("The array Elements are :");
                for(int i=0; i<size; i++){
                        System.out.print(arr[i]);
			if(i==size-1){
				break;
			}
			System.out.print(",");
                }
                System.out.println();
        }
}
