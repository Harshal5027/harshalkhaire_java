import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the count of the Employee : ");
		int count=sc.nextInt();
		int arr[]=new int[count];

		System.out.println("Enter the elements : ");
		for(int i=0; i<count; i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("The ages of the given Employee are : ");
		for(int i=0; i<count; i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();

	}
}

