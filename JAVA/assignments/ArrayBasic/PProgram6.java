import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an Array : ");
		int size=sc.nextInt();
		char arr[]=new char[size];

		System.out.println("Enter the characters : ");
		for(int i=0; i<size; i++){
			arr[i]=sc.next().charAt(0);
		}

		System.out.print("The given characters are : ");
		for(int i=0; i<size; i++){
			System.out.print(arr[i]+" ");
		}
	}
}
