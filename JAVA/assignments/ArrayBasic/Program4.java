import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an Array : ");
		int size=sc.nextInt();
		int sum=0;
		int arr[]=new int[size];

		System.out.println("Enter array Element");
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
		}
		System.out.println("Sum of the odd element is : "+sum);
	}
}
