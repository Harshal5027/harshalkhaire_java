class SwitchDemo{
	public static void main(String[]args){

		char ch='O';

		switch(ch){
			case 'O':
				System.out.println("OutStanding");
				break;

			case 'A':
				System.out.println("Excellent");
				break;

			case 'B':
				System.out.println("B");
				break;

			case 'C':
				System.out.println("C");
				break;

			case 'D':
				System.out.println("D");
				break;

			case 'F':
				System.out.println("Fail");
				break;

			default :
				System.out.println("Default");
		}
	}
}

