import java.util.*;
class TriangleDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter number of rows : ");
                int row= sc.nextInt();

                for(int i=1; i<=row; i++){
                        int num1=65+row-i;
			int num2=97+row-i;

                        for(int j=1; j<=row-i+1; j++){
                                if(row%2==0){
					if(i%2==0){
                                        	System.out.print((char)num2 +" ");
					}else{
						System.out.print((char)num1 +" ");
					}
					num1--;
					num2--;
				}else{
					if(i%2==0){
						System.out.print((char)num1 +" ");
					}else{
						System.out.print((char)num2 +" ");
					}
					num1--;
					num2--;
				}
			}
			System.out.println();
		}
	}
}
