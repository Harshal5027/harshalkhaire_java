import java.util.*;
class TriangleDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter number of rows : ");
                int row= sc.nextInt();
		
		int num=row*(row+1);
                for(int i=1; i<=row; i++){

                        for(int j=1; j<=row-i+1; j++){
				System.out.print(--num + " ");
				num--;
                        }
                        System.out.println();
                }
        }
}
