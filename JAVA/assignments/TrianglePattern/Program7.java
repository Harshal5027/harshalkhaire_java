import java.util.*;
class TriangleDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter number of rows : ");
                int row= sc.nextInt();

                for(int i=1; i<=row; i++){
                        int num=row-i+1;
                        int ch=97+row-i;

                        for(int j=1; j<=row-i+1; j++){
                                if(j%2==0){
                                        System.out.print((char)ch +" ");
                                }else{
                                        System.out.print(num +" ");
                                }
				num--;
				ch--;

                        }
                        System.out.println();
                }
        }
}
