import java.util.*;
class ArrayDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size=sc.nextInt();
                int arr[]=new int[size];

                System.out.print("Enter elements : ");
                for(int i=0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }

                int sum=0;
                System.out.print("Elements divisible by 3 ");
                for(int i=0; i<arr.length; i++){
                        if(arr[i]%3==0){
                                System.out.print(arr[i]+" ");
                                sum=sum+arr[i];
                        }

                }
                System.out.println();
                System.out.println("Sum of element divisible by 3 is : "+ sum);
        }
}
