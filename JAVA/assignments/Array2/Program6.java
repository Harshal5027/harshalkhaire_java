import java.util.*;
class ArrayDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter the size of an array : ");
                int size=sc.nextInt();
                int arr[]=new int[size];

                int product=1;
                System.out.println("Enter elements : ");
                for(int i=0; i<arr.length;i++){
                        arr[i]=sc.nextInt();
                        if(i%2==1){
                                product=product*arr[i];
                        }
                }
                System.out.println("Output : ");
                System.out.print("Product of the odd indexed elements is : " + product );
                System.out.println();
        }
}
