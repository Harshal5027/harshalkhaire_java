import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of Array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.print("Enter elements ");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.next().charAt(0);
		}

		System.out.println("Enter the number to search in array : ");
		int num=sc.nextInt();
		System.out.print("Output : ");
		for(int i=0; i<arr.length; i++){
			if(num==arr[i]){
				System.out.println(num+" found at index "+i);
			}
		}
		System.out.println();
	}
}

