import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.print("Enter the elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		
		if(arr.length%2==0){
			System.out.println("Array elements are : ");
			for(int i=0; i<arr.length; ){
				System.out.println(arr[i]);
				i+=2;
			}
		}else{
			System.out.println("Array elements are : ");
			for(int i=0; i<arr.length; i++){
				System.out.println(arr[i]);
			}
		}
	}
}
			
