import java.util.*;
class ArrayDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter the size of an arrar : ");
                int size=sc.nextInt();
                int arr[]=new int[size];

                System.out.println("Enter the elements : ");
                for(int i=0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }

                int max=arr[0];
		int index=0;
                for(int i=0; i<arr.length; i++){
                        if(max<arr[i]){
                                max=arr[i];
				index=i;
                        }
                }
                System.out.println("Maximum number in the array is found at position "+index+" is "+max);
        }
}
