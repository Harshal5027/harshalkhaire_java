import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		
		int sum=0;
		System.out.println("Enter elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(i%2==1){
				sum=sum+arr[i];
			}
		}
		System.out.println("Output : ");
		System.out.print("Sum of the odd indexed elements is : " + sum );
		System.out.println();
	}
}
