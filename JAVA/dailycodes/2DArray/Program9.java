import java.util.*;
class ArrayDemo{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter the size : ");
                int size=sc.nextInt();
		int sum=0;

                int arr[]=new int[size];
                System.out.println("Size of an array : "+arr.length);

                for(int i=0; i<size;i++){
                        System.out.print("Enter Element : ");
                        arr[i]=sc.nextInt();
                }
                System.out.println("Array element are: ");
                for(int i=0; i<size; i++){
                        System.out.println(arr[i]);
			sum=sum+arr[i];
                }
		System.out.println("Sum : "+sum);
        }
}
